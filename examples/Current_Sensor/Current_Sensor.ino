/*
 * @file Current_Sensor.ino
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Simple example of current sensor utilisation.
 */
 
#include <NT_Current_Sensor.h>

Current_Sensor_t CurrentSensor1;

void setup()
{      
  CurrentSensor1.Configure(0, 1023, 0, 5);  
  CurrentSensor1.Attach(A0);  
  CurrentSensor1.Init();
}

void loop()
{
  Serial.print("Sensor current : ");
  Serial.print(CurrentSensor1.Get());
  Serial.println(" Amps");
}
