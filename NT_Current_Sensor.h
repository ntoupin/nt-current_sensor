/*
 * @file NT_Current_Sensor.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Current sensor function declarations.
 */

#ifndef NT_Current_Sensor_h

#include "Arduino.h"

#define TRUE 1
#define FALSE 0

#define NT_Current_Sensor_h

class Current_Sensor_t
{
public:
	Current_Sensor_t();
	bool Enable;
	float Raw_Value; // Adc
	float Current_Value; // Amps
	int Minimum_Adc;
	int Maximum_Adc;
	int Minimum_Current;
	int Maximum_Current;
	void Configure(int Minimum_Adc, int Maximum_Adc, int Minimum_Current, int Maximum_Current);
	void Attach(int Pin);
	void Init();
	void Deinit();
	float Get();
	
private:
	int _Pin;	
};

#endif