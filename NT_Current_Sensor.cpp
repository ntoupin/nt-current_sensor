/*
 * @file NT_Current_Sensor.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Current sensor function definitions.
 */

#include "Arduino.h"
#include "NT_Current_Sensor.h"

Current_Sensor_t::Current_Sensor_t()
{
}

void Current_Sensor_t::Configure(int Minimum_Adc, int Maximum_Adc, int Minimum_Current, int Maximum_Current)
{
	Minimum_Adc = Minimum_Adc;
	Maximum_Adc = Maximum_Adc;
	Minimum_Current = Minimum_Current;
	Maximum_Current = Maximum_Current;
}

void Current_Sensor_t::Attach(int Pin)
{
	_Pin = Pin;
}

void Current_Sensor_t::Init()
{
	Enable = TRUE;
}

void Current_Sensor_t::Deinit()
{
	Enable = FALSE;
}

float Current_Sensor_t::Get()
{
	Raw_Value = analogRead(_Pin);
	Current_Value = map(Raw_Value, Minimum_Adc, Maximum_Adc, Minimum_Current, Maximum_Current);

	return Current_Value;
}